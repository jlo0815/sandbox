/**
 * 
 */
package fm.austria.sandbox.dynabean;

import org.springframework.context.annotation.Scope;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedMetric;
import org.springframework.jmx.export.annotation.ManagedResource;

/**
 * @author developer
 *
 */
@ManagedResource(objectName="metric:name=myMetric")
@Scope("prototype")
public class Metric {
	
	RestResource resource;
	
	public Metric(RestResource resource) {
		this.resource=resource;
	}

	@ManagedMetric
	public int getCounter() {
		return resource.getCount();
	}
	
	@ManagedAttribute
	public String getName() {
		return resource.getName();
	}
	
	@ManagedAttribute()
	public void setMessage(String message) {
		resource.setMessage(message);
	}
	
	@ManagedAttribute
	public String getMessage() {
		return resource.getMessage();
	}
	
	
	

}
