package fm.austria.sandbox.dynabean;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.jmx.export.annotation.AnnotationMBeanExporter;

@SpringBootApplication
public class DynabeanApplication {
	
	
	
	
	
	@Bean
	@Scope("prototype")
	public Metric metricProducer(RestResource res) {
		Metric m=new Metric(res);
		mbeanExporter().registerManagedResource(m);
		return m;
		
	}
	
	@Bean
	public MetricFactory metricFactory() {
		return new MetricFactory() {
			
			@Override
			public Metric createMetric(RestResource restResource) {
				return metricProducer(restResource);
			}
		};
	}
	
	@Bean
	public AnnotationMBeanExporter mbeanExporter() {
		AnnotationMBeanExporter mbeanExporter=new AnnotationMBeanExporter();
		mbeanExporter.setAutodetect(false);
		return mbeanExporter;
	}
	

	public static void main(String[] args) {
		SpringApplication.run(DynabeanApplication.class, args);
	}

}
