/**
 * 
 */
package fm.austria.sandbox.dynabean;

/**
 * @author developer
 *
 */
public interface MetricFactory {
	
	public Metric createMetric(RestResource restResource);

}
