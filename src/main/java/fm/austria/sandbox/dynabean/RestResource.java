/**
 * 
 */
package fm.austria.sandbox.dynabean;

/**
 * @author developer
 *
 */
public class RestResource {
	
	public RestResource(String name) {
		super();
		this.name = name;
		this.message="hi from "+name;
		this.count=0;
	}

	private String name;
	private int count;
	private String message;
	
	
	
	
	public int incrementCount() {
		return this.count++;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getName() {
		return name;
	}

	public int getCount() {
		return count;
	}

	

}
