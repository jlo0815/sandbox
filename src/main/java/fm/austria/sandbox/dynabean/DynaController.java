/**
 * 
 */
package fm.austria.sandbox.dynabean;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author developer
 *
 */
@RestController
@RequestMapping("/test")
public class DynaController {

	Map<String, RestResource> resourceMap = new HashMap<>();

	@Autowired
	private MetricFactory metricFactory;

	@GetMapping("/{path}")
	public RestResource get(@PathVariable("path") String path) {
		RestResource rr = resourceMap.get(path);
		if (rr == null) {
			rr = new RestResource(path);
			resourceMap.put(path, rr);
			Metric m = metricFactory.createMetric(rr);
		}
		rr.incrementCount();
		return rr;
	}

}
